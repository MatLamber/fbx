using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    [SerializeField] private GameObject jugador;
    
    private Animator _controladorAnimacion;
    private Rigidbody _jugadorBody;
    void Awake()
    {
        _controladorAnimacion = this.gameObject.GetComponent<Animator>();
        _jugadorBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 9)
        {
            Debug.Log("Hubo Colision (Rampa)");
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.layer == 9)
        {
            Debug.Log("en el aire");
            _jugadorBody.useGravity = true;
          Invoke("ApagarGravedad",5);

        }
    }

    private void ApagarGravedad()
    {
        _jugadorBody.useGravity = false;
    }
}
