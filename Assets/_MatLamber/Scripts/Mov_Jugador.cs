using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Jugador : MonoBehaviour
{
    [SerializeField] private GameObject jugador; //Modelo/Prefab del jugador
    [SerializeField] private float velocidadMovimiento; //La velocidad con la que se realiza el "swerve"
    [SerializeField] private float velocidadCorriendo;
    [SerializeField] private float movimientoMaximo;//Con este campo se limita la amplitud maxima del "swerve"
    private float _ultimaPosicionX;//Se guarda la ultima posicion en la que quedo el puntero desde que presiono click/toco la pantalla
    private float _movimientoRealizado;//Se guarda la informacion del movimiento (si fue a la derecha,izquierda o nulo)
    private Rigidbody _jugadorBody;//Fisicas del movimiento
    private CharacterController _jugadorController;
    
    private void Awake()
    {
        _jugadorBody = jugador.GetComponent<Rigidbody>(); // Se accede al componente de fisicas del movimiento
        _jugadorController = jugador.GetComponent<CharacterController>();
    }

    private void Update()
    {
        CalcularMovimiento();
        RealizarMovimiento();

    }

    private void CalcularMovimiento()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _ultimaPosicionX= Input.mousePosition.x; //Posicion del mouse al presionar click
        }
        else if (Input.GetMouseButton(0))
        {
            _movimientoRealizado = Input.mousePosition.x - _ultimaPosicionX; // Si el moviemiento fue a la derecha o izquierda
            _ultimaPosicionX = Input.mousePosition.x; // Se actualiza la posicoin actual del curos/dedo
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _movimientoRealizado = 0f; //Si deja de presionar click/tocar la pantalla se toma como que no hubo movimiento
        }
    }

    private void RealizarMovimiento()
    {
        float movimiento = Time.deltaTime * velocidadMovimiento * _movimientoRealizado; //Direccion y velocidad del movimiento
        movimiento = Mathf.Clamp(movimiento, -movimientoMaximo, movimientoMaximo);//Se fuerza a que quede en el limite establecido de velocidad
        _jugadorBody.AddForce(movimiento, 0, 0); // se aplica la fuerza
    }
}
