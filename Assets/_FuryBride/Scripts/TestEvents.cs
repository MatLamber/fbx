using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TestEvents : MonoBehaviour
{
    [SerializeField] private Text textMsg;
    
    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Instance.ActionTestEmptyEvent += OnTestEmptyEvent;
        EventsManager.Instance.ActionTestWithParams += OnTestWithParams;
        EventsManager.Instance.ActionTestWithMultipleParams += OnTestWithMultipleParams;
    }

    private void OnDestroy()
    {
        EventsManager.Instance.ActionTestEmptyEvent -= OnTestEmptyEvent;
        EventsManager.Instance.ActionTestWithParams -= OnTestWithParams;
        EventsManager.Instance.ActionTestWithMultipleParams -= OnTestWithMultipleParams;
    }

    private void OnTestEmptyEvent()
    {
        textMsg.text = "OnTestEmptyEvent";
    }

    private void OnTestWithParams(int value)
    {
        textMsg.text = $"OnTestWithParams value = {value}";
    }

    private void OnTestWithMultipleParams(Transform valueTransform, int valueInt, string valueStr)
    {
        textMsg.text =
            $"OnTestWithMultipleParams valueTransform = {valueTransform.name} & valueInt = {valueInt} & valueStr = {valueStr}";
    }
}
