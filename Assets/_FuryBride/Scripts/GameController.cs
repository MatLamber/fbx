using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    private Camera _mainCamera;
    public Camera GetCamera() => _mainCamera;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            _mainCamera = Camera.main;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        
    }

    
}
