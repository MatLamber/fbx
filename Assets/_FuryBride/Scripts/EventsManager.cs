using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager : MonoBehaviour
{
    public static EventsManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    public event Action ActionTestEmptyEvent;
    public event Action<int> ActionTestWithParams;
    public event Action<Transform, int, string> ActionTestWithMultipleParams;
    
    public void OnTestEmptyEvent() => ActionTestEmptyEvent?.Invoke();
    public void OnTestWithParams(int valueInt) => ActionTestWithParams?.Invoke(valueInt);
    public void OnTestWithMultipleParams(Transform valueTransform, int valueInt, string valueStr) => ActionTestWithMultipleParams?.Invoke(valueTransform, valueInt, valueStr);
}
