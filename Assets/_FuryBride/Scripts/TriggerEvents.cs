using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvents : MonoBehaviour
{
    public void ClickBtEvents()
    {
        EventsManager.Instance.OnTestEmptyEvent();
    }

    public void ClickBtParams()
    {
        EventsManager.Instance.OnTestWithParams(7);
    }

    public void ClickBtMultipleParams()
    {
        EventsManager.Instance.OnTestWithMultipleParams(transform, 88, "start");
    }
}
